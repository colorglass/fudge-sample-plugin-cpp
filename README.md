# Fully Dynamic Game Engine C++ Sample Plugin
A sample SKSE plugin developed in C++, built on the Fully Dynamic Game Engine platform for Skyrim Special Edition.

## Table of Contents
- [Fully Dynamic Game Engine C++ Sample Plugin](#fully-dynamic-game-engine-c-sample-plugin)
  - [Table of Contents](#table-of-contents)
  - [Getting Started](#getting-started)
    - [Environment Setup](#environment-setup)
      - [Installing Visual Studio](#installing-visual-studio)
      - [Installing/Configuring Visual Studio Code](#installingconfiguring-visual-studio-code)
      - [Installing Git](#installing-git)
      - [Vcpkg Install and Configuration](#vcpkg-install-and-configuration)
    - [Cloning the Repository](#cloning-the-repository)
    - [Importing the Project into Your IDE](#importing-the-project-into-your-ide)
      - [Visual Studio](#visual-studio)
      - [Visual Studio Code](#visual-studio-code)
    - [Setting Up Debugging](#setting-up-debugging)
  - [Understanding the Project Structure](#understanding-the-project-structure)
    - [Vcpkg Setup](#vcpkg-setup)
    - [CMake](#cmake)
      - [Vcpkg Configuration](#vcpkg-configuration)
      - [Build Options](#build-options)
      - [Post-Build Automatic Deployment](#post-build-automatic-deployment)
    - [Plugin Declaration](#plugin-declaration)
    - [Config File Support](#config-file-support)
    - [Logging](#logging)
    - [Papyrus Bindings and Script Objects](#papyrus-bindings-and-script-objects)
    - [Save Game Hooks](#save-game-hooks)
    - [Function/Call Hooks](#functioncall-hooks)
    - [Declarative Event Handling](#declarative-event-handling)
    - [Custom Console Commands](#custom-console-commands)
    - [Unit Testing](#unit-testing)
    - [Papyrus Development](#papyrus-development)

## Getting Started
### Environment Setup
#### Installing Visual Studio
To do Windows development you will need to install [Visual Studio](https://visualstudio.microsoft.com/). The Community
Edition is free to install, but you must create a Visual Studio account. During install you will be presented with
the components you wish to install for development. The only one required for SKSE development is "Desktop development
with C++". Select it and leave the detailed options on the right untouched unless you really know what you are doing.

![Visual Studio Installer](docs/visual-studio-install.png)

#### Installing/Configuring Visual Studio Code
The Visual Studio installer includes the Visual Studio IDE as well as the development tools needed for C++ development.
However, many SKSE developers use Papyrus as well, since SKSE can be used to add new Papyrus bindings. This is typically
done using Visual Studio Code as it has advanced plugins for Papyrus development other IDEs lack. You can [download
Visual Studio Code](https://visualstudio.microsoft.com/) for free at the same site you used to get Visual Studio.

Once installed, open Visual Studio Code. On the left-hand side of the window find the button called Extensions and click
it (or press `Ctrl+Shift+X`). Search for "Papyrus" in the search bar at the top of the panel and find the extension
called "Papyrus" by Joel Day. Click "Install" to install the extension. You will now be able to setup Papyrus
development workspaces in VS Code.

![Visual Studio Code Papyrus Extension Install](docs/vscode-papyrus-install.png)

#### Installing Git
If you do not already have Git installed, [download and install it](https://gitforwindows.org/) (you do not need to
worry about the specific configuration options during install).

#### Vcpkg Install and Configuration
Vcpkg is a package manager for C/C++ libraries, which makes integrating third-party libraries into your project easy. It
is also installed with Git. Clone Vcpkg and then set it up by running the following commands:

```commandline
git clone https://github.com/microsoft/vcpkg
.\vcpkg\bootstrap-vcpkg.bat
.\vcpkg\vcpkg integrate install
```

This project allows for using default Vcpkg configuration when none is externally specified (e.g. from the command line
or when built as a dependency via Vcpkg). This makes development in your dev environment simpler. To auto-detect Vcpkg
you must set an environment variable `VCPKG_ROOT` to the path to your Vcpkg install. To do so open Control Panel and
go to System. On the left-hand side click About. You will now see an option on the right-hand side of the window for
"Advanced system settings". You will get a window with a number of options; click "Environment Variables".

![Environment Variables Button](docs/vcpkg-system-properties.png)

In the environment variables screen click New and enter `VCPKG_ROOT` as the variable name. For the value, enter the full
path to your Vcpkg install. Note that this variable is not picked up by currently-running applications, until they are
restarted.

![Environment Variables Settings](docs/vcpkg-env.png)

### Cloning the Repository
Clone this repository to your local machine by running
the following command at the command line (using Command Prompt, Powershell, or Windows Terminal):

```commandline
git clone https://gitlab.com/colorglass/fudge-sample-plugin-cpp.git
```

This will create a directory called `fudge-sample-plugin-cpp` with a clone of this project.

### Importing the Project into Your IDE
#### Visual Studio
Open Visual Studio. You will be presented with a launch screen that lets you select the project to work on. You want to
select "Open a local folder".

![Visual Studio Import](docs/visual-studio-import-folder.png)

Find the folder to which you cloned this repository and select that folder. Visual Studio should open and you will
shortly see a screen that should look roughly like the following (note your window may vary in the visual theme, icons,
and placement of the panels, such as the file tree being on the right side rather than left; this is because my own
installation I used to take these screenshots is not fresh and has been customized).

![Visual Studio Project Imported](docs/visual-studio-freshly-imported.png)

Visual Studio will begin to import the CMake configuration. CMake is build system used by this project, and it is
configured in the `CMakeLists.txt` file. Visual Studio will automatically begin to download and build all the
dependencies needed to build this project, and import the projects settings. This will take some time. If you do not
see the "Output" panel, look around the edge of the screen for a tab that says "Output" and click it to see the output
from the CMake import process. Wait until it is done. If you open files before it is complete Visual Studio cannot tell
you yet if there are any problems, and you will see a warning along the top of the file that C++ Intellisense is not yet
available.

![Visual Studio CMake Import In Progress](docs/visual-studio-cmake-processing.png)

Once Visual Studio has completed the import of the project, you can now do development. You will now have an option to
build the project under the Build menu. Use `Build->Build All` (or `Ctrl+Shift+B`) to build the project.

![Visual Studio CMake Import In Progress](docs/visual-studio-cmake-success.png)

Build the project with `Build->Build All` (or `Ctrl+Shift+B`); if all has gone well you should see a notification that
the build was successful in your status bar, and in the output panel if it is visible. Congratulations, you've built
your first SKSE plugin! You can find the DLL in the project directory under `build/FDGESamplePlugin.dll`.

![Visual Studio CMake Import In Progress](docs/visual-studio-build-success.png)

#### Visual Studio Code

### Setting Up Debugging
To be able to debug your project you must use a debug version of SKSE and a debuggable version of the Skyrim executable.
By default Skyrim cannot be debugged because the Steam DRM will kill the process when it detects the process breaking at
a breakpoint. To work around this you can use [Steamless](https://github.com/atom0s/Steamless), a tool which strips
Steam DRM from an executable. Download and extract Steamless and run it, and point it at your `SkyrimSE.exe` file. Run
Steamless with default options and it will produce an unpacked executable. Backup your original executable rename that
file to `SkyrimSE.exe` (note: another option is to copy the unpacked file, renamed `SkyrimSE.exe` into a Mod Organizer 2
mod if you use the Root Builder plugin; this process is not covered here).

![Unpacking Steam DRM with Steamless](docs/steamless.png)

You will need a debug build of SKSE as well. If you are using this project, a debug version will be built for you and
present at `build/debug/vcpkg_installed/x64-windows-skse/bin/skse64_1_6_353.dll`. There you will also find
`skse64_loader.exe` and `skse64_steam_loader.dll`. In addition you will find `.pdb` files for all of these. Copy these
to your Skyrim directory (or again, a Mod Organizer 2 mod if you use Root Builder). You now have a debug version of SKSE
in use, with a debuggable version of Skyrim. With a debug SKSE DLL, SKSE will delay starting up until a debugger
attaches to the Skyrim process. When Skyrim starts, attach a debugger (for example, in Visual Studio, use
Debug->Attach To Process and find `SkyrimSE.exe`) to let Skyrim start up. If you delay for too long SKSE will fail the
startup. You can now place break points and debug your plugin.

## Understanding the Project Structure
###  Vcpkg Setup
To properly link against the dependencies you will need a custom Vcpkg triplet. Triplets define the target platform and
whether a library is linked against statically or dynamically. For SKSE development it is important that most libraries
be linked against statically; if linked dynamically those libraries' DLLs must be included in the SKSE plugins
directory, which will generate a lot of noise in SKSE log files warning about DLLs being found which are not SKSE
plugins.

There are however exceptions to this rule. If you are directly linking against SKSE (not recommended for modern
SKSE development and not done this project), or linking against another SKSE plugin that is acting as a library (such as
Fully Dynamic Game Engine is used here), those should be linked dynamically. To handle this a custom triplet has been
placed in the `cmake` directory, called `x64-windows-skse`, which implements such exceptions. The CMake definition
adds this directory as a place to find triplets via the `VCPKG_OVERLAY_TRIPLETS` property.

In addition, to use the custom dependencies not found in the official Vcpkg repository, a custom repository has been
defined in `vcpkg-configuration.json`. This includes Fully Dynamic Game Engine and its dependencies, including a port of
CommonLibSSE, which thanks to this repository can now be consumed as a Vcpkg package.

Finally, we have defined this project as a Vcpkg in `vcpkg.json`. This includes our dependencies and other
configuration. Of note here is that this project is defined to have multiple *features*. The core feature is the SKSE
plugin, but in addition two more features are defined: one to build the tests, and one to distribute the Papyrus scripts
defined in this project. If your plugin is intended to be consumed by others, then this allows you to exclude tests from
the version that third-parties will install, as well as distribute your Papyrus script sources to them to compile their
own scripts against.

### CMake
#### Vcpkg Configuration
Vcpkg and the target triplet/linking behavior is automatically configured for this project. When Vcpkg configuration is
not explicit when running CMake, the Vcpkg toolchain file is discovered via the environment variable `VCPKG_ROOT`. This
variable is commonly used in many open source projects to detect and automatically configure their projects, and this
plugin follows that convention. This simplifies your IDE setup by not requiring manual configuration. The correct
triplet is also configured (see [Vcpkg Setup](#vcpkg-setup) for more information), and static linking is configured
using either the debug or release MSVC runtimes, depending on whether the project is built in debug or release mode.

The CMake configuration is handled by `CMakePresets.json`, a configuration file that is standard in CMake and supported
by IDEs such as Visual Studio, Visual Studio Code, and CLion. This generates build configurations in your IDE in a
standardized way. The presets included `x64-windows-skse` triplet for Vcpkg builds described above. The release build
uses the `RelWithDebInfo` build type, which will also generate a PDB file. If it is not too large it is recommended to
distribute the PDB with your plugin when it is posted to Nexus (or if it is quite large, consider providing it as a
separate optional download). Having a PDB files can help debugging tools and crash loggers get more detailed information
about problems, and FuDGE can use it to get file name and line number information for native stack frames when creating
a Papyrus `Error` instance.

#### Build Options
The build has optional test compilation.

```cmake
message("Options:")
option(BUILD_TESTS "Build unit tests." OFF)
message("\tTests: ${BUILD_TESTS}")
```

This allows tests to be conditionally included in the build by passing `-DBUILD_TESTS` to CMake.

#### Post-Build Automatic Deployment
The CMake build includes an optional post-build auto-deployment feature. If you have set an environment variable
`FDGESampleTarget` then the compiled DLL and it's debugging symbols (the PDB file) will be copied to that directory.
This can be a path to your `Data/SKSE/Plugins` directory or the equivalent under a Mod Organizer 2 mod to auto-update
your plugin for testing after every build.

```cmake
if (WIN32 AND DEFINED ENV{${PROJECT_NAME}Target})
  message("Adding build copy target $ENV{${PROJECT_NAME}Target}.")
  add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:${PROJECT_NAME}> $ENV{${PROJECT_NAME}Target}
      )
  add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_PDB_FILE:${PROJECT_NAME}> $ENV{${PROJECT_NAME}Target}
      )
endif ()
```

Note that if you are building both release and debug configurations whichever completes second will be the one that ends
up in the target directory.

### Plugin Declaration
The plugin is declared in `src/Main.cpp`. Fully Dynamic Game Engine has a declarative plugin definition system. This is
based on the system used by SKSE in the post-AE versions. Unlike raw SKSE plugins it is not required to assign it via an
explicit lambda execution, and no `SKSEPlugin_Load` function is required.

```c++
SKSEPlugin(
        Name = "Sample Plugin";
        Version = {1, 0, 0};
        Author = "Charmed Baryon";
        Email = "charmedbaryon@elderscrolls-ng.com";
        PreInit = []() {
            FDGE::Logger::Initialize(Config::GetProxy());
            Config::GetProxy().StartWatching();
        };
);
```

Most properties correspond to those in the existing SKSE plugin definition. The `SKSESerializationPluginID` defines the
record (the four characters) that will be used to save this plugin's data in the SKSE cosave, when using SKSE's save
game integration. The `PreInit` property is a function that will be run immediately upon plugin initialization, before
the rest of the initialization takes place. It is primarily for initializing logging systems so that logging can already
be present to log the initialization process itself.

### Config File Support
The configuration for this plugin is defined in `src/Config.h`. FuDGE provides a powerful and exensible config system
that can source data from files or any other data source (if an implementation exists). The bridge between a data source
and the data is called a "proxy". This example plugin uses the `DynamicFileProxy`, which is given a file name without an
extension and looks for a config file with that base name with one of its supported extensions. Based on the extension
it finds it will determine the expected file format. This lets the plugin support configuration in multiple formats,
such as YAML, JSON, TOML, and others.

The framework which reads the config file is, by default, Articuno. Articuno is a powerful serialization framework that
can read data even into fully functioning (non-POCO, for C++ techies) class structures. In this case we are using the
default base class for plugin configuration from FuDGE, which comes with logging configuration. FuDGE provides a
powerful logging system that can be used with this logging configuration.

FuDGE focuses heavily on being dynamic, and configs are no exception. FuDGE's config system can react to changes to the
config file at runtime. We see this being enabled in the `PreInit` function of the plugin declaration, where we call
`StartWatching` on the config proxy. This makes the proxy watch the underlying file for changes. When a change happens
an event is emitted, and event listeners can update the various plugin systems in reaction.

```c++
PreInit = []() {
    FDGE::Logger::Initialize(Config::GetProxy());
    Config::GetProxy().StartWatching();
};
```

### Logging
As mentioned above, FuDGE has a logging system that can be initialized if basing a plugin's configuration on that of
FuDGE's base config class. We see this being initialized in the `PreInit` function of the plugin declaration in
`src/Main.cpp`.

```c++
PreInit = []() {
    FDGE::Logger::Initialize(Config::GetProxy());
    Config::GetProxy().StartWatching();
};
```

Calling `FDGE::Logger::Initialize` starts the logging system. This should be done in `PreInit` to get logging for the
initialization process itself. The FuDGE logging system supports file logging (generally to `Documents\My Games\Skyrim
Special Edition\SKSE`, the standard location for SKSE plugin logs), debug logging (which will output logs to your IDE
when you have attached a debugger), and Windows event logging (viewable in the Windows Event Viewer). A config file is
included at `contrib/FDGESamplePlugin.yaml` that explicitly lists all the default values for logging, but note that if
the config file is absent or any values are not included in the config file the default value is assumed.

The logging system is fully dynamic. If you have called `StartWatching` on the proxy, the logger will react to update
events and reconfigure itself, allowing the log configuration to be changed at runtime.

### Papyrus Bindings and Script Objects
FuDGE provides a cleaner, declarative way to bind Papyrus calls to native functions. Papyrus bindings do not need to be
invoked via a registration function which must be called from the SKSE plugin's initialization function, allowing for
an inversion of control pattern in which various modules of the project can be completely independent of each other
with no coupling between them.

An additional major feature of FuDGE is that it allows, for the first time, true use of custom Papyrus types. These
types do not need to be attached to an existing game object such as a form or magic effect, but rather can be
arbitrarily created at any time, and are automatically garbage collected their reference count reaches zero. These
types do not need to be managed via an untyped integer handle as has been done in the past, but have full typing like
any other Papyrus script, and allow for invoking member functions, accessing properties, and changing states.

An example of this system is found in `HitCounter.h` and `HitCounter.cpp`. Looking at `HitCounter.h` shows how a
custom script type is created.

```c++
class HitCounter : public FDGE::Binding::Papyrus::ScriptObject {
        ScriptType(HitCounter);
     // ...
}
```

The above declaration defines a custom script type backed by a C++ class, a hit counter (which will record how many
times an actor gets hit). Two important features of any C++ class backing a script type is it must descend from
`FDGE::Binding::Papyrus::ScriptObject`, and it must provide both static and per-member access to the name of the
Papyrus type it backs. The later condition is easily met with the `ScriptType` macro, which takes the name of the
Papyrus class associated with the type as an argument.

Custom script types must be serializable so they can be stored in the FuDGE cosave. The serialization system is
strictly limited to Articuno, a next-generation serialization framework that uses standard formats, generally of the
JSON/YAML object model (a full description of how to use Articuno is beyond the scope of this document).

```c++
class HitCounter : public FDGE::Binding::Papyrus::ScriptObject {
    // ...
private:
    articuno_serde(ar) {
        ar <=> articuno::kv(_target, "target");
        ar <=> articuno::kv(_count, "count");
    }
    
    RE::Actor* _target{nullptr};
    std::atomic_int _count{0};

    friend class articuno::access;
}
```

Note that serialization functions can be private by making `articuno::access` a friend class. Also note that it is
possible to directly serialize pointers to forms, active effects, and aliases, as well as serialize other script objects
by reference when using `ScriptObjectHandle<T>`. If a script object saves a reference to another script object, it must
be held as a `ScriptObjectHandle<T>` to ensure a reference is kept in the reference count, preventing it from being
freed by the Papyrus VM. A handle that does not hold a reference is a `WeakScriptObjectHandle<T>`, which must have
`Lock()` called on it to convert it back to a `ScriptObjectHandle<T>` to work on the object.

To bind this type we need a Papyrus class that defines the functions we will expose to Papyrus.

```papyrus
scriptName HitCounter extends ScriptObject hidden

HitCounter function Create(Actor target) global native

Int function GetTotalHitCounters() global native

function Increment(Int by = 1) native

Int property Count
    Int function get()
        return __GetCount()
    endFunction
endProperty
Int function __GetCount() native
```

Note that we need to have a constructor function, since this class is created arbitrarily, not attached to existing
objects via a record in an ESP file, and hence the `Create` function, which is a global one. The other functions can be
normal member function calls. We can also have properties on our Papyrus class, however properties cannot be bound to
native functions. To work around this the convention is to create a pseudo-private function to back the property, in
this case `__GetCount()`, which can be bound to native. The property is defined to call the function for its getter.

In `HitCounter.cpp` we ensure the custom type is registered with the Papyrus VM, and the Articuno serialization
system within FuDGE:

```c++
RegisterScriptType(HitCounter)
```

This macro takes the name of the C++ class (not the Papyrus class, although it is recommended to name them the same when
possible) as its argument. We can use FuDGE's declarative Papyrus binding system, and use raw pointers to the
`HitCounter` class like we would any other type supported natively in CommonLibSSE's binding system:

```c++
PapyrusClass(HitCounter) {
    PapyrusStaticFunction(Create, Actor* target) -> HitCounter* {
            if (!target) {
                return nullptr;
            }
            auto* existing = HitCounterManager::GetSingleton().GetHitCounter(target);
            if (existing) {
                return existing;
            }
            return new HitCounter(target);
    };

    PapyrusFunction(Increment, HitCounter* self, int by) {
        if (self) {
            return self->Increment(by);
        }
    };

    PapyrusFunction(__GetCount, HitCounter* self) {
        if (!self) {
            return 0;
        }
        return self->Get();
    };
}
```

As we can see, creating the new script object is as simple as calling `new` to create it and returning the pointer. **It
is vital that `new` be used to instantiate the script object and not an alternate allocation method.** FuDGE must be
able to free your allocated memory when the Papyrus object associated with the C++ class is garbage collected. FuDGE
overloads the `new` and `delete` operators on `ScriptObject` to ensure all script objects are allocated from the shared
process heap when using these operators, rather than your SKSE DLL's private heap. This ensures FuDGE has access to free
the memory from its own DLL.

### Save Game Hooks
FuDGE comes with an improved cosave system over SKSE, which simplifies adding your own data to a save game. As with
other FuDGE features this is designed to support large-scale plugins with multiple moving parts without requiring any
coupling. As such any object can register itself as participating in the cosave, and it will do so as long as the
object exists. Unlike traditional SKSE plugins there is no need for a single global serialization handler to know about
all participants in the cosave and call each one.

The script object management system that backs custom Papyrus types is one such cosave participant, but plugins can add
their own. In this example we have the `HitCounterManager`, a class which tracks all existing hit counters so they can
be made singleton per-actor, and looked by actor. This class needs to save its state in the cosave, and so it inherits
from `FDGE::Hook::SerializationHook`:

```c++
class HitCounterManager : public FDGE::Hook::SerializationHook {
    // ...
    
protected:
    void OnNewGame() override;

    void OnGameSaved(std::ostream& out) override;

    void OnGameLoaded(std::istream& in) override;

    // ...
};
```

The virtual functions `OnNewGame`, `OnGameSaved`, and `OnGameLoaded` can handle relevant events for persistence. The
save/load functions accept standard C++ streams for reading and writing data, which may be in any format desired. In
addition two functions are not used here: `OnGameSaving` and `OnGameLoading`. These handle persistent data as well, but
in the *pre-cosave` rather than the `post-cosave`. Traditional SKSE cosave data is loaded after the core Skyrim save
file is loaded, as is the case with `OnGameSaved` data which is loaded by `OnGameLoaded`. However any data saved via
`OnGameSaving` will only be visible to the `OnGameLoading` callback, and it will be loaded before Skyrim loads its own
save file. This can be important if you need to configure something in the engine custom to the state of the game that
the engine will need for loading its own data. This pre-cosave is not needed in this example project and so no override
is needed for these functions. In this case, we have used Articuno to read/write the data we need, and done so by
directly serializing the `HitCounterManager` itself:

```c++
void HitCounterManager::OnNewGame() {
    _hitCounters.clear();
}

void HitCounterManager::OnGameSaved(std::ostream &out) {
    yaml_sink ar(out);
    ar << *this;
}

void HitCounterManager::OnGameLoaded(std::istream &in) {
    in.clear();
    yaml_source ar(in);
    ar >> *this;
}
```

Finally, we ensure that when constructing our object it has a unique name with which to associate its data, which can
be done by invoking the parent constructor with a `string` or `string_view`. Unlike SKSE cosaves, FuDGE cosaves use a
string of arbitrary length rather than being restricted to four bytes.

```c++
HitCounterManager::HitCounterManager() : SerializationHook("HitCounterManager"sv) {
    ScriptObjectStore::GetSingleton().ListenForever([this](const ScriptObjectCreatedEvent& event) {
        auto* hitCounter = dynamic_cast<HitCounter*>(event.GetObject());
        if (hitCounter) {
            _hitCounters.try_emplace(hitCounter->GetTarget(), hitCounter);
        }
    });

    ScriptObjectStore::GetSingleton().ListenForever([this](const ScriptObjectBoundEvent& event) {
        auto* hitCounter = dynamic_cast<HitCounter*>(event.GetObject());
        if (hitCounter) {
            result.first->second->DecRef();
        }
    });

    ScriptObjectStore::GetSingleton().ListenForever([this](const DestroyingScriptObjectEvent& event) {
        auto* hitCounter = dynamic_cast<HitCounter*>(event.GetObject());
        if (hitCounter) {
            _hitCounters.erase(hitCounter->GetTarget());
        }
    });
}
```

Also note that this constructor registers listeners on the `ScriptObjectStore`. These events allow any component to
eavesdrop on script object creation and deletion. A common use case is to detect when an object is deleted because all
references have been removed. Also note that in this example, the `HitManagerCounter` stores a
`WeakScriptObjectHandle<HitCounter>` to track the object. As noted above this is a safe way to store a script object and
allows serialization by reference, rather than value. The handle is a weak handle since we do not want this class to
prevent the reference count from reaching zero via losing all references in Papyrus classes, as that would prevent
garbage collection.

### Function/Call Hooks
FuDGE has a declarative system for function and call hooking as well. In this example, we need to know when a hit has
landed to attempt to increment any relevant hit counters. This is done by hooking a function with FuDGE's
`FDGE::Hook::FunctionHook<T>` class. This class is templated by the function's type. The function hook can take a
pointer to the memory address to hook, although this is not preferred since function addresses change between Skyrim
executable updates. Address library is preferrable, and providing any integer will be interpreted as an address library
ID. In addition, FuDGE provides a class called an `AddressID` which can abstract address library IDs to make them
portable across address library lineages (Skyrim SE prior to the Anniversary Edition release, Skyrim SE versions after
the Anniversary Edition release, and Skyrim VR). This allows for code bases that are portable across multiple Skyrim
versions. In this example we are using `AddressID` and have provided the SE and AE IDs (VR edition is not yet supported
in this example).

```c++
static FunctionHook<void(HitData*, Actor*, Actor*, InventoryEntryData*)> PopulateHitData(
    AddressID::Create(44001, 42832),
        [](HitData* self, Actor* aggressor, Actor* target, InventoryEntryData* inv) {
            PopulateHitData(self, aggressor, target, inv);
            HitCounterManager::GetSingleton().RegisterHit(target);
        });
```

The `AddressID::Create` call here creates an abstraction for working with AE address library ID 44001, and SE ID 42832.
Our template indicates that the signature of the function being hooked is `void(HitData*, Actor*, InventoryEntryData*)`.
Because this is a function hook, this hooks the function being called, and thus this will intercept any call to this
function from anywhere. The more selective `CallHook` and `BranchHook` are not exemplified here, but they allow hooking
the call site itself, replacing a single call to redirect to your hook without affecting other calls to the same
function.

This hook is declared `static` so that it is globally accessible. This makes it accessible in its own hook function, the
lambda which we passed to the hook. Invoking the hook object itself calls the original function. In this case our hook
simply intercepts the call, passes the arguments on to the original function, but before returning then records the hit
to any relevant hit counter.

Function hooks are usually permanent, but if you wish to unhook a function, the hook is tied to the lifetime of the
object. If the `FunctionHook` leaves scope or is reassigned to a default state, the hook will be unregistered.

### Declarative Event Handling
In typical SKSE development event handling is centralized. SKSE loading is handled by a single function,
`SKSEPlugin_Query`, and further event handling between SKSE and the plugin (or multiple plugins) is done by setting
your plugin's message handler. For large projects this creates coupling as a main component must know all other
components to refer any messages or events to them.

FuDGE allows for declarative event handling with no coupling. Any source file can declare a handler to run during the
SKSE loading process, e.g.:

```c++
OnSKSELoading {
    // Do something.
}
```

Within this handler the `LoadInterface` macro provides access to the SKSE load interface, in the rare event it is
needed. The type of the function is void. Throwing any C++ or structured Windows error will result in your plugin
not being loaded; for AE editions of SKSE this produces a popup warning the user and giving them an option to kill
the process. If you wish to terminate loading without this popup, then you can throw a `FDGE::PluginIncomptible`
exception.

The `OnSKSELoading` handlers run first in the load process (after your plugin's `PreInit` handler), but in no
determinate order relative to each other. To add a handler that runs last, use `OnSKSELoaded`. You can also declare
`OnSKSELoad(n)`, where `n` is an unsigned integer representing the priority of the handler. The lower the priority the
earlier the handler will run.

If you want a handler to run after all SKSE plugins are initialized (usually the first place you want to do anything
significant with the engine, and also where multithreading becomes safe), you can use the `AfterSKSEPluginsLoaded`
handler. We see this used in this example plugin to initialize our function hook:

```c++
AfterSKSEPluginsLoaded {
        static FunctionHook<void(HitData*, Actor*, Actor*, InventoryEntryData*)> PopulateHitData(...);
}
```

It is also possible to use a similar system for handling messages from `SKSE::MessagingInterface`. The
`OnSKSEMessage(type, n)`, where `type` is the message type constant and `n` is a priority. For example:

```c++
OnSKSEMessage(SKSE::MessagingInterface::kDataLoaded, 10) {
    // ...
}
```

For the lowest priority (run last) you can also use `OnSKSEMessageReceived(type)` and for the highest priority (run
first) you can also use `OnSKSEReceivingMessage(type)`. However SKSE's messaging interface is not favored in FuDGE
development for SKSE events; rather FuDGE provides it's own event dispatcher system and the object `FDGE::System`
can be used to register for these events, with more features.

### Custom Console Commands
FuDGE has a framework for true custom console commands, not merely console commands that alter existing seldom-used
ones. The framework also has built-in support for GNU-style arguments and parameters.

In this example project a simple console command demo is included in `ListActors.cpp`. This adds a `listactors` command
that can be used to list all currently active actors in the loaded areas. This all demos the use of FuDGE's exposure of
the engine's actor iterator functionality. In this example we are using a *static command*, which defines a console
command that is automatically registered on startup and remains for the lifetime of the program. These are the most
common, and simplest, commands.

```c++
FDGECommand(sample, listactors, lsacts) {
    // ...
}
```

This macro declares a console command with a namspace of "sample" and the names "listactors" and "lsacts". The namespace
and at least one name is required, but there is no limit on how many name aliases can be included. Traditionally console
commands have a full name and optionally a short version (e.g. `togglegodmode` and `tgm`). The namespace is used to
disambiguate the command being used if two mods try to register a command of the same name. The namespace is not
required for using an unambiguous command. If needed, the command can be invoked in a fully qualified why by using the
syntax `namespace::command` (`sample::listactors` in this example).

The macro `Context` can be used in the command body to access the parsed data for the command.

### Unit Testing
If unit tests are enabled in the build, then a test executable will be created.

```cmake
if (BUILD_TESTS)
  enable_testing()

  add_executable(
          ${PROJECT_NAME}Tests
          ${headers}
          ${tests}
  )

  target_compile_features(${PROJECT_NAME}Tests
          PUBLIC
          cxx_std_23
          )

  target_link_libraries(
          ${PROJECT_NAME}Tests
          PRIVATE
          ${PROJECT_NAME}
          GTest::gmock
          GTest::gtest
          GTest::gmock_main
          GTest::gtest_main
  )

  target_precompile_headers(${PROJECT_NAME}Tests
          PRIVATE
          src/PCH.h
          )

  gtest_discover_tests(${PROJECT_NAME}Tests)
endif ()
```

Tests are built on the Google Test framework, and dummy sample tests are included under the `test` directory. If you
add a new test file you must add it to the CMake definition for `tests`. The output will be `FDGESamplePluginTests.exe`
and running that executable runs the tests. Google Test support is built into Visual Studio and other IDEs such as
CLion, and therefore can be run directly in the IDE to see test results.

### Papyrus Development
The sample project includes configuration for a Visual Studio Code workspace that can utilize the Papyrus plugin. It is
setup for the automated Papyrus script extraction done via Vcpkg using the custom repository, and therefore can compile
against Papyrus sources from vanilla Skyrim, SKSE, and Fully Dynamic Game Engine.

The project is made to utilize the Papyrus extension for Visual Studio Code. Opening the `.code-workspace` file
included with the project will enable Papyrus development. The project is configured to take in Papyrus scripts
installed with the Vcpkg install. Build output will be placed in `build/debug/papyrus` and
`build/relwithdebinfo/papyrus`, depending on whether building for debug of release.
