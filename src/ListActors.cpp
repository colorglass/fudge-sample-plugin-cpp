#include <FDGE/Hopesfire.h>

using namespace FDGE::Console;
using namespace FDGE::Skyrim;

namespace {
    FDGECommand(sample, listactors, lsacts) {
        Context.NoExplicitTarget();

        auto includeEditorIDs = Context.Flag("editorid", "e");
        auto includeFullNames = Context.Flag("name", "n");
        auto max = Context.NextOptionalPositional(std::numeric_limits<uint32_t>::max());

        uint32_t count = 0;
        for (auto actorHandle : RE::ProcessLists::GetSingleton()->highActorHandles) {
            auto actor = actorHandle.get();
            if (!actor) {
                continue;
            }
            if (count++ >= max) {
                return;
            }

            if (includeEditorIDs) {
                if (includeFullNames) {
                    Print(R"({:X} (Editor ID = "{}", Name = "{}"))", actor->GetFormID(), actor->GetFormEditorID(),
                          actor->GetDisplayFullName());
                } else {
                    Print(R"({:X} (Editor ID = "{}"))", actor->GetFormID(), actor->GetFormEditorID());
                }
            } else if (includeFullNames) {
                Print(R"({:X} (Name = "{}"))", actor->GetFormID(), actor->GetDisplayFullName());
            } else {
                Print("{:X}", actor->GetFormID());
            }
        }
    }
}
