#include <FDGE/Hopesfire.h>
#include <Sample/PluginInfo.h>

#include "SampleConfig.h"

using namespace FDGE;
using namespace Sample;

SKSEPlugin(
        Name = "Sample Plugin";
        Version = {1, 0, 0};
        Author = "Charmed Baryon";
        Email = "charmedbaryon@elderscrolls-ng.com";
        SKSESerializationPluginID = "SMPL";
        PreInit = []() {
            Logger::Initialize(SampleConfig::GetProxy());
            SampleConfig::GetProxy().StartWatching();
        };
);
