#include <Sample/HitCounter.h>

#include <RE/F/FormTraits.h>
#include <Sample/HitCounterManager.h>

using namespace FDGE::Hook;
using namespace Sample;
using namespace RE;

namespace {
    AfterSKSEPluginsLoaded {
        static FunctionHook<void(HitData*, Actor*, Actor*, InventoryEntryData*)> PopulateHitData(
                RELOCATION_ID(42832, 44001),
                    [](HitData* self, Actor* aggressor, Actor* target, InventoryEntryData* inv) {
                        PopulateHitData(self, aggressor, target, inv);
                        HitCounterManager::GetSingleton().RegisterHit(target);
                    });
    }

    RegisterScriptType(HitCounter)

    PapyrusClass(HitCounter) {
        PapyrusStaticFunction(Create, Actor* target) -> HitCounter* {
                if (!target) {
                    return nullptr;
                }
                auto* existing = HitCounterManager::GetSingleton().GetHitCounter(target);
                if (existing) {
                    return existing;
                }
                return new HitCounter(target);
        };

        PapyrusFunction(Increment, HitCounter* self, int by) {
            if (!self) {
                return 0;
            }
            return self->Increment(by);
        };

        PapyrusFunction(__GetCount, HitCounter* self) {
            if (!self) {
                return 0;
            }
            return self->Get();
        };
    }
}

