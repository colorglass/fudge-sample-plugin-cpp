#pragma once

/**
 * Including Hopesfire also includes Black Book and Trueflame, as well as all necessary dependencies. It also includes
 * CommonLibSSE and guarantees it will be included without problems regardless of issues with Windows headers being
 * included. This ensures all your source files will have access to everything they need. It is also recommended to
 * simply include Hopesfire.h in your header files to get easy access to everything you need.
 *
 * In this PCH file, include any additional third-party dependencies after this line (note all standard C++ libraries
 * are also already included and precompiled).
 */
#include <FDGE/Hopesfire.h>
