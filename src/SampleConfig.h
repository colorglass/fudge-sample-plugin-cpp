#pragma once

#include <FDGE/Hopesfire.h>

namespace Sample {
    class SampleConfig : public FDGE::Config::DefaultPluginConfig {
    public:
        [[nodiscard]] static inline FDGE::Config::Proxy<SampleConfig>& GetProxy() noexcept {
            static FDGE::Config::DynamicFileProxy<SampleConfig> proxy(L"Data\\SKSE\\Plugins\\FDGESample");
            return proxy;
        }
    };
}
