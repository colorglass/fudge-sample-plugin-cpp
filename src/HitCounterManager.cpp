#include <Sample/HitCounterManager.h>

using namespace articuno;
using namespace articuno::ryml;
using namespace FDGE::Binding::Papyrus;
using namespace FDGE::Hook;
using namespace gluino;
using namespace RE;
using namespace Sample;

HitCounterManager::HitCounterManager() : SerializationHook("HitCounterManager"sv) {
    ScriptObjectStore::GetSingleton().ListenForever([this](const ScriptObjectBoundEvent& event) {
        auto* hitCounter = dynamic_cast<HitCounter*>(event.GetObject());
        if (hitCounter) {
            _hitCounters.try_emplace(hitCounter->GetTarget(), hitCounter);
        }
    });

    ScriptObjectStore::GetSingleton().ListenForever([this](const DestroyingScriptObjectEvent& event) {
        auto* hitCounter = dynamic_cast<HitCounter*>(event.GetObject());
        if (hitCounter) {
            _hitCounters.erase(hitCounter->GetTarget());
        }
    });
}

HitCounterManager& HitCounterManager::GetSingleton() noexcept {
    static HitCounterManager instance;
    return instance;
}

void HitCounterManager::OnNewGame() {
    _hitCounters.clear();
}

void HitCounterManager::OnGameSaved(std::ostream &out) {
    yaml_sink ar(out);
    ar << *this;
}

void HitCounterManager::OnGameLoaded(std::istream &in) {
    in.clear();
    yaml_source ar(in);
    ar >> *this;
}

void HitCounterManager::RegisterHit(RE::Actor* target) noexcept {
    _hitCounters.if_contains(target, [](const auto& weakHandle) {
        auto hitCounterHandle = weakHandle.second.Lock();
        if (!hitCounterHandle) {
            return;
        }
        auto* hitCounter = hitCounterHandle.GetObject();
        if (hitCounter) {
            hitCounter->Increment();
        }
    });
}

HitCounter* HitCounterManager::GetHitCounter(RE::Actor *target) const noexcept {
    HitCounter* result = nullptr;
    _hitCounters.if_contains(target, [&](const auto& weakHandle) {
        return weakHandle.second.Lock().GetObject();
    });
    return result;
}

namespace {
    AfterSKSEPluginsLoaded {
        discard(HitCounterManager::GetSingleton());
    }
}
