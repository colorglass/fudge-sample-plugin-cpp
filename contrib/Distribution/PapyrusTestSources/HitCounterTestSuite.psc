scriptName HitCounterTestSuite extends AutoTestSuite

function TestNoneConstructor()
    Assert.IsObjectNone(HitCounter.Create(None))
endFunction

function TestConstructorForActor()
    HitCounter counter = HitCounter.Create(Game.GetPlayer())
    Assert.IsObjectNotNone(counter)
    Assert.AreIntsEqual(0, counter.Count)
endFunction

function TestIncrement()
    HitCounter counter = HitCounter.Create(Game.GetPlayer())
    Assert.IsObjectNotNone(counter)
    Int count = counter.Count
    counter.Increment()
    Assert.AreIntsEqual(count + 1, counter.Count)
endFunction

function TestIncrementMultiple()
    HitCounter counter = HitCounter.Create(Game.GetPlayer())
    Assert.IsObjectNotNone(counter)
    Int count = counter.Count
    counter.Increment(3)
    Assert.AreIntsEqual(count + 3, counter.Count)
endFunction

function TestSingletonPerActor()
    HitCounter counter = HitCounter.Create(Game.GetPlayer())
    Assert.IsObjectNotNone(counter)
    Assert.AreObjectsEqual(counter, HitCounter.Create(Game.GetPlayer()))
endFunction
