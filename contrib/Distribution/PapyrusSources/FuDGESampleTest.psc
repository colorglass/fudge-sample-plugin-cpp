scriptName FuDGESampleTest extends Quest

Actor property Player auto
HitCounter counter
Int iterations = 0

event OnInit()
    Utility.Wait(0.1)
    counter = HitCounter.Create(Player)
    RegisterForSingleUpdate(30.0)
endEvent

event OnUpdate()
    iterations = iterations + 1
    Debug.Notification("Player has been hit " + counter.Count + " times.")
    if iterations < 10
        RegisterForSingleUpdate(30.0)
    endIf
endEvent
